#!/bin/bash
# Return the number of unreaded messages in your Gmail account
# It requires generate the permissions and obtain a special pass from Gmail
mail=`curl -su $your_mail_username:$your_mail_pass \
      https://mail.google.com/mail/feed/atom/^sq_ig_i_personal ||
      echo "<fullcount>unknown number of</fullcount>"`
mail=`echo "$mail" | grep -oPm1 "(?<=<fullcount>)[^<]+" `
[[ $mail =~ ^[0-9]+$ ]] || mail=0

[[ $mail == 0 ]] && echo -e "\x01 🖃 $mail "
[[ $mail != 0 ]] && echo -e "\x03 🖃 $mail \x01"
