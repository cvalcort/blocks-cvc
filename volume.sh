#!/bin/bash
# Return the volume level
mute ()
{
	[[ $(echo $(pactl list sinks | grep Mute:)) == "Mute: yes" ]] && echo off
	[[ $(echo $(pactl list sinks | grep Mute:)) == "Mute: no" ]] && echo on
}

[[ $(mute) == on ]] &&
      vol=$(pactl list sinks | grep '^[[:space:]]Volume:' | head -n $(( $SINK + 1 )) | tail -n 1 | sed -e 's,.* \([0-9][0-9]*\)%.*,\1,')
[[ $(mute) == off ]] && vol="MUTE"
echo " 🔊 $vol "
