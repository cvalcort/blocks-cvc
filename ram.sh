#!/bin/bash
# Return the $ram left and the $swap usage
ram=$(free -m | awk '$1 == "Mem:" {printf "%.1f", ($2 - $3) / 1000}')
swap=$(free -m | awk '$1 == "Swap:" {printf "%.1f", $3 / 1000}')
echo " RAM $ram SWP $swap "
