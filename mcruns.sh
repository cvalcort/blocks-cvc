#!/bin/bash
dd=$server1
ll=$server2
ww=$server3
mm=$server4
vv=$server5
v=$(ssh -q $vv 'squeue -t RUNNING | sed ''/JOBID/d'' | wc -l')
dn1=$(ssh -q $dd 'ps aux' |
      awk '$11 ~ "penEasy.x" {print $0}' | wc -l)
dn2=$(ssh -q $dd "ssh -q node02 'ps aux'" |
    awk '$11 ~ "penEasy.x" {print $0}' | wc -l)
dn3=$(ssh -q $dd "ssh -q node03 'ps aux'" |
    awk '$11 ~ "penEasy.x" {print $0}' | wc -l)
dn4=$(ssh -q $dd "ssh -q node04 'ps aux'" |
    awk '$11 ~ "penEasy.x" {print $0}' | wc -l)
# l=$(ssh -q $ll 'ps aux' |
#     awk '$11 ~ "penEasy.x" {print $0}' | wc -l)
# w=$(ssh -q $ww 'ps aux' | awk '$11 ~ "penEasy.x" {print $0}' |
#     wc -l)
# m=$(ssh -q $mm 'ps aux' |
#     awk '$11 ~ "penEasy.x" {print $0}' | wc -l)
td=$(($dn1 + $dn2 + $dn3 + $dn4))
echo " V$v/D$td " #/W$w/L$l/M$m "
