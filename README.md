# blocks-cvc
Scripts to use with my [dwmblocks build](https://gitlab.com/cvalcort/dwmblocks-cvc.git). Their should installed in the path specified in the [blocks.h](https://gitlab.com/cvalcort/dwmblocks-cvc.git) file

Remove mcout.sh and mcruns.sh from this folder and from my [blocks.h](https://gitlab.com/cvalcort/dwmblocks-cvc.git), which are
useless to everyone but me.

arch_updates.sh works only for archlinux users

