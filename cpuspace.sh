#!/bin/bash
# Return the $space left in the hard drive and the $cpu usage
space=$(df | awk '$1 == "/dev/sda3" {printf "%.1f", $4 / 1000000}')
cpu=$(top -bn1 | grep "Cpu(s)" | sed "s/.*, *\([0-9].*\)%* id.*/\1/" |
      awk '{print 100 - $1"%"}')
echo " 💻 $cpu 🖴 $space "
