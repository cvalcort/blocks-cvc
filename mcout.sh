expdir=$mc_run_dir
ssh -qn $server1 'cd ~/current_sims/;./imp.sh'
scp -q $server1:~/current_sims/tally_comb_cpp.dat $expdir
mc=$(awk 'NR == 19 {printf "%.3E (%.2f\%)", $2, $3 / $2 * 100}'\
     $expdir/tally_comb_cpp.dat)
echo " $mc "
