#!/bin/bash
# Return the number of packages that need to be updated through pacman -Syu
# safely without root privileges
TMPPATH="${TMPDIR:-/tmp}/checkup-db-${USER}"
DBPATH="$(pacman-conf DBPath)"

mkdir -p "$TMPPATH"
ln -s "$DBPATH/local" "$TMPPATH" &>/dev/null
fakeroot -- pacman -Sy --dbpath "$TMPPATH" --logfile /dev/null &>/dev/null
a=$(pacman -Qu --dbpath "$TMPPATH" 2>/dev/null | wc -l)
[[ $a == 0 ]] && echo -e "\x01 📦 $a "
[[ $a != 0 ]] && echo -e "\x03 📦 $a \x01"
