# Return the bright level (in percentage)
bright=$(xrandr --verbose | awk '$0 ~ "Brightness" {print $2 * 100}')
echo " ☀ $bright "
